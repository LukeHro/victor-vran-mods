--LootItem(1, NewItem("Spear_Sucuba", {}),true)

DefineClass.Spear_Sucuba = {
  __parents = {"Spear"},
  name = T({"Sucuba Spear"}),
  entity = "Sucuba_Spear",
  container = "LootHammer_HeroHammer",
  ui_image = "sucuba_spear.tga",
  rarity = 4,
  damage_prc = 30,
  mod1 = "efficient",
  mod1_prc = 150,
  mod2 = "vulture",
  mod2_prc = 50,
  mod3 = "leg_sucuba_spear"
}

WeaponModifiers.leg_sucuba_spear = {
  description = T({"<purple>Thrown spear pierce through multiple targets</purple>"}),
  param_min = 100,
  param_max = 100,
  cost_boost_min = 150,
  cost_boost_max = 200
}

DefineClass.Spear_Basic = {
  __parents = {"Spear"},
  entity = "Sucuba_Spear",
  container = "LootHammer_HeroHammer",
  ui_image = "basic_spear.tga",
  rarity = 1,
}

function Spear_Basic:CreateObject(luaobj)
  local obj = Weapon.CreateObject(self, luaobj)
  obj:SetColorModifier(RGB(40,20,10))  
  return obj
end

local VendorLootTable_GenWeapon = function(classname, mods)
  return function(level, seed)
    return GenerateWeapon(classname, nil, level, seed)
  end
end


function OnMsg.ClassesBuilt()
  -- TheHunter.slots = {
  --     function(level)
  --       return NewItem("BasicSpear", {
  --         level = level,
  --         damage_prc = 20,
  --         crit_prc = 60,})
  --     end,
  --     table.unpack(TheHunter.slots)
  --   }
  TheAlchemist.slots = {VendorLootTable_GenWeapon("Spear_Basic"), table.unpack(TheAlchemist.slots)}
  DemonVendor.slots = {"Spear_Sucuba", table.unpack(DemonVendor.slots)}
end