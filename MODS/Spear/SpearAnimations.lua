
local StateSet_Init = StateSet.Init
function StateSet:Init( ... )
  StateSet_Init(self, ...)
  if (self.file_name=="Hero") then
    self.so_states.Spear_Attack1 = PlaceObj("State", {
      "name", "Spear_Attack1",
      "owner", "Hero",
      "inherit", "Base_Attack",
      "animation", "scytheReap2",
      "animation_blending", "yes",
      "animation_speed", "100",
      "movement", "Attack",
      "target", "Attack",
      "action", "SA_Spear_Attack1",
      "next_state", "Idle"
    })
    self.so_states.Spear_Attack2 = PlaceObj("State", {
      "name", "Spear_Attack2",
      "owner", "Hero",
      "inherit", "Base_Attack",
      "animation", "rapierAttack3",
      "animation_blending", "yes",
      "animation_speed", "100",
      "animation_step", "200",
      "movement", "Attack",
      "target", "Attack",
      "action", "SA_Spear_Attack2",
      "next_state", "Idle",
      "override_moments", "yes",
      "animation_start", "-1",
      "animation_hit", "733",
      "animation_end", "800"
    })
    self.so_states.Spear_Attack3 = PlaceObj("State", {
      "name", "Spear_Attack3",
      "owner", "Hero",
      "inherit", "Base_Attack",
      "animation", "swordAttack3",
      "animation_speed", "50",
      "animation_step", "70",
      "movement", "Attack",
      "target", "Attack",
      "action", "SA_Spear_Attack3",
      "next_state", "Idle",
      "override_moments", "yes",
      "animation_start", "-1",
      "animation_hit", "133",
      "animation_end", "300"
    })
    self.so_states.Spear_Attack4 = PlaceObj("State", {
      "name", "Spear_Attack4",
      "owner", "Hero",
      "inherit", "Base_Attack",
      "animation", "swordAttack7",
      "animation_speed", "50",
      "animation_step", "70",
      "movement", "SlideForwardAttack",
      "action", "SA_Spear_Attack4",
      "next_state", "Idle"
    })
    self.so_states.Spear_Wall = PlaceObj("State", {
      "name", "Spear_Wall",
      "owner", "Hero",
      "animation", "revolverBulletHell",
      "state_lifetime", "movement",
      "movement", "SpearWall",
      "target", "Attack",
      "action", "SA_Spear_Wall",
      "next_state", "Idle"
    })
    self.so_states.Spear_Throw = PlaceObj("State", {
      "name", "Spear_Throw",
      "owner", "Hero",
      "inherit", "Base_Attack",
      "animation", "swordAttack3",
      "movement", "Attack",
      "target", "Attack",
      "action", "SA_Spear_Throw",
      "next_state", "Idle"
    })
  end
end