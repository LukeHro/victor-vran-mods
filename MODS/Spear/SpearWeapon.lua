local CurrentModPath = CurrentModPath or ""

DefineClass.Spear = {
  __parents = {"Weapon"},
  base_name = T({"Spear"}),
  weapon_type = "Hammer",
  fx_actor_base_class = "Hammer",
  ui_color = "Blue",
  ui_image = "spear.tga",
  states_prefix = "Hammer",
  main_skills = {
    "SA_Spear_Attack",
    "SA_Spear_Wall",
    "SA_Spear_Throw"
  },
  main_skill1_variants = {
    "SA_Spear_Attack1",
    "SA_Spear_Attack2",
    "SA_Spear_Attack3",
    "SA_Spear_Attack4"
  },
  description = T({"The spear is a slow but quite powerfull usefull in both close and ranged combat"}),
  weapon_tip = T({"Keep your enemies at distance with the Spear Wall and kill them from distance or charge through them when the opportunity shows up"}),
  codex_entry = "Hammer",
  codex_order = 3,
  move_anim = "hammerRun",
  idle_anim = "hammerIdle",
  weapon_spot = "Weapon1",
  holster_spot = "Weapon1back",
  base_damage = 50,
  base_damage_bonus = 20,
  critical_chance = 15,
  critical_bonus = 100,
  armor_penetration = 50,
  critical_min = 10,
  critical_max = 20,
  CostBaseDamage = 600,
  CostBonusDamage = 150
}
function Spear:GetImage()
  return CurrentModPath..self.ui_image
end

function Spear:ChooseAttackAction(trigger, obj)
  local skill, hold_attack
  if trigger == "Attack2" then
    skill = self.main_skills[2]
  elseif trigger == "Attack3" then
    skill = self.main_skills[3]
  elseif not obj:HasCooldown("Spear_Attack") then
    if trigger ~= "Attack1" then
      hold_attack = obj:GetStateContext("Attack1")
    end
    if trigger == "Attack1" or hold_attack then
      skill = "SA_Spear_Attack1"
      local cur_action = obj.so_action
      if cur_action == SA_Spear_Attack1 or obj:HasCooldown("Spear_Attack2") then
        skill = "SA_Spear_Attack2"
      elseif cur_action == SA_Spear_Attack2 or obj:HasCooldown("Spear_Attack3") then
        skill = "SA_Spear_Attack3"
      elseif cur_action == SA_Spear_Attack3 or obj:HasCooldown("Spear_Attack4") then
        skill = "SA_Spear_Attack4"
      end
    end
  end
  return skill and _G[skill], hold_attack
end
function Spear:OnSkillHit(obj, skill)
end

DefineClass.SA_Spear_Attack = {
  __parents = {"HeroAction"},
  name = T({"Attack"}),
  description = T({"A chain of attacks ending with a powerfull charge through enemies leaving them <lightgreen2><condition></lightgreen2>.\n\n<lightgreen2><condition></lightgreen2>: <description>"}),
  GetDescription = function(self, unit)
    return T({
      self.description,
      condition = Buff_Exposed.name,
      description = Buff_Exposed:GetDescription(unit)
    })
  end,
  icon = CurrentModPath.."hud_spear_01.tga",
  main_skill_idx = 1,
  fx_action_base = "Hammer_Attack",
  fx_weapon_actor = true,
  attack_weapon = "Hammer",
  weapon = "Hammer",
  weapon_spot = "Weapon1",
  best_range = 150 * guic,
  follow_target = true,
  melee_attack = true,
  slice_attack = true,
  goto_attack = true,
  target_trigger = "PainInterrupt",
  hit_range = 340 * guic,
  max_slide = 3 * guim,
  slide_moment = "StartToHit",
  basic_attack = true
}
function SA_Spear_Attack:Hit(obj)
  local weapon = obj:GetActiveWeapon()
  if weapon then
    weapon:OnSkillHit(obj, self)
  end
  self:MeleeAttackSingleTarget(obj)
end

DefineClass.SA_Spear_Attack1 = {
  __parents = {
    "SA_Spear_Attack"
  },
  statename = "Spear_Attack1"
}
function SA_Spear_Attack1:End(obj)
  if not obj.so_enabled then
    return
  end
  obj:SetCooldown("Spear_Attack2", ModifyAttackSwingCooldown(obj, 260))
end

DefineClass.SA_Spear_Attack2 = {
  __parents = {
    "SA_Spear_Attack"
  },
  statename = "Spear_Attack2"
}
function SA_Spear_Attack2:Start(obj)
  obj:RemoveCooldown("Spear_Attack2")
end
function SA_Spear_Attack2:End(obj)
  if not obj.so_enabled then
    return
  end
  obj:SetCooldown("Spear_Attack3", ModifyAttackSwingCooldown(obj, 290))
end

DefineClass.SA_Spear_Attack3 = {
  __parents = {
    "SA_Spear_Attack"
  },
  statename = "Spear_Attack3"
}
function SA_Spear_Attack3:Start(obj)
  obj:RemoveCooldown("Spear_Attack3")
end
function SA_Spear_Attack3:End(obj)
  if not obj.so_enabled then
    return
  end
  obj:SetCooldown("Spear_Attack4", ModifyAttackSwingCooldown(obj, 600))
end

DefineClass.SA_Spear_Attack4 = {
  __parents = {
    "SA_Spear_Attack"
  },
  statename = "Spear_Attack4",
  attack_moment = "start",
  hit_range = 100 * guic,
  max_slide = 50 * guic,
  cooldown_id = "Spear_Attack4",
  target_buff = "Buff_Exposed",
  attack_area = "circle",
  attack_radius = 50 * guic,
  slide_dist = 5 * guim,
  goto_attack = false,
  goto_point = false,
}
function SA_Spear_Attack4:Start(obj)
  obj:RemoveCooldown("Spear_Attack4")
  local damaged = {}
  obj:StateRepeat(50, self.AreaDamage, nil, nil, nil, damaged, damaged)
end
function SA_Spear_Attack4:Hit(obj)
  obj:SetCooldown("Spear_Attack", ModifyAttackSwingCooldown(obj, 500))
  SA_Spear_Attack.Hit(self, obj)
end

DefineClass.SpearWallProjectile = {
  __parents = {"Projectile"},
  action = "SA_Spear_Wall",
  entity = "Sucuba_Spear",
  fx_actor_entity = "Sucuba_Spear",
  shooter_offset = point(700, 100, 10),
  speed = 5 * guim,
  hit_rangeZ = 500 * guic,
  travel_step = 20 * guic,
  travel_distance = 20 * guim,
  front_reduction = 100,
  target_enemy_classes = false,
  target_enemy_flags_any = const.efUnit + const.efAttackableObject + const.efTargetableProjectile
}
SpearWallProjectile.Move = Projectile.MoveStraight
SpearWallProjectile.HitTargetCheck = Projectile.HitCheckClosestEnemy
function SpearWallProjectile:MoveStep(stepx, stepy, stepz, t) 
  return Projectile.MoveStep(self,0,0,0,t)
end

DefineClass.SA_Spear_Wall = {
  __parents = {
    "SA_Tome_BaseAttack"
  },
  statename = "Spear_Wall",
  name = T({"Spear Wall"}),
  description = T({"Surounds yourself with a circle of spears that damages enemies that come close and leaves them <lightgreen2><condition></lightgreen2>.\n\n<lightgreen2><condition></lightgreen2>: <description>"}),
  GetDescription = function(self, unit)
    return T({
      self.description,
      condition = Buff_OpenWounds.name,
      description = Buff_OpenWounds:GetDescription(unit)
    })
  end,
  attack_moment = "hit-moment1",
  icon = CurrentModPath.."hud_spear_02.tga",
  cooldown = 10000,
  cooldown_id = "Spear_Wall",
  target_buff = "Buff_OpenWounds",
  attack_pos_as_trigger_obj = true,
  attacks_count = 16,
  projectile_class = "SpearWallProjectile",
  adm = 300,
  range_attack = true,
  max_range = 12 * guim,
  fx_action_base = "Hammer_Attack",
  fx_weapon_actor = true,
  attack_weapon = "Hammer",
  weapon = "Hammer",
  weapon_spot = "Weapon1",
  follow_target = true
}
function StateMoveSystems.SpearWall(obj)
  obj.so_action_param = obj.so_action_param or {}
  local stateidx = obj.so_changestateidx
  local start_phase = obj:GetStateAnimPhase("Start")
  local end_phase = obj:GetStateAnimPhase("End")
  local x, y = obj:GetVisualPosXYZ()
  obj:SetPos(x, y, GetStepZ(x, y))
  obj:FaceTargetOrAttackDirection()
  obj:WaitPhase(start_phase)
  obj:StateActionMoment("action")
  if stateidx ~= obj.so_changestateidx then
    return
  end
  local opos = obj:GetVisualPos()
  local weapon = obj:GetActiveWeaponObj()
  local fire_spot_pos = weapon and (weapon:GetSpotLoc(weapon:GetSpotBeginIndex("Origin")) + point(1500, 0, -1500))
  local fire_dist_2d = fire_spot_pos:Len2D()
  local start_angle = obj:GetAngle() + obj:AngleToPoint(fire_spot_pos)
  local fire_offset = Rotate(fire_spot_pos - opos, -start_angle)
  obj:StateActionMoment("hit-moment1", nil, weapon)
  local count = obj.so_action.attacks_count
  for i = 1, count do
    local attack_angle = start_angle - (i - 1) * 360 * 60 / count
    local fire_pos = opos + Rotate(fire_offset, attack_angle)
    local target_pos, collision_pos = obj.so_action:GetRangeAttackTargetPos(obj, fire_pos, nil, attack_angle)
    local axis, angle = OrientationToAxisAngle((fire_pos - opos):SetZ(0), point30, axis_z, 1)
    local projectile  = NetPlaceProjectile("SpearWallProjectile", obj, nil, fire_pos, angle, axis)
    local pos = projectile:GetPos() 
    local dx, dy = projectile:RotateAxisXYZ(4096, 0, 0)
    local angle = atan(dy, dx)
    if not obj:GetActiveWeapon():IsKindOf("Spear_Sucuba") then
      projectile:SetColorModifier(RGB(40,20,10))
    end
    projectile:FacePos(ChooseAttackZ(pos, angle, projectile.travel_distance, pos:z(), projectile.max_z_angle):xyz())
    obj:StateActionMoment("hit-moment", nil, weapon)
    if stateidx ~= obj.so_changestateidx then
      return
    end
    attack_angle = attack_angle + 10800
    obj:WaitPhase(start_phase + i * (end_phase - start_phase) / count)
  end
  obj:StateActionMoment("post-action")
  if stateidx ~= obj.so_changestateidx then
    return
  end
  obj:WaitPhase(obj:GetLastPhase())
end

DefineClass.SpearProjectile = {
  __parents = {"Projectile"},
  action = "SA_Spear_Throw",
  entity = "Sucuba_Spear",
  fx_actor_entity = "Sucuba_Spear",
  shooter_offset = point(700, 100, 1600),
  speed = 30 * guim,
  travel_distance = 20 * guim,
  target_enemy_flags_any = const.efUnit + const.efAttackableObject + const.efTargetableProjectile
}
SpearProjectile.Move = Projectile.MoveStraight
SpearProjectile.HitTargetCheck = Projectile.HitCheckClosestEnemy

function SpearProjectile:GameInit()
  self:SetScale(120)
  self:SetColorModifier(RGB(40,20,10))
end

DefineClass.SucubaSpearProjectile = {
  __parents = {"Projectile"},
  action = "SA_Spear_Throw",
  entity = "Sucuba_Spear",
  fx_actor_entity = "Sucuba_Spear",
  shooter_offset = point(700, 100, 1600),
  speed = 30 * guim,
  travel_distance = 20 * guim,
  target_enemy_flags_any = const.efUnit + const.efAttackableObject + const.efTargetableProjectile
}
SucubaSpearProjectile.Move = Projectile.MoveStraight
SucubaSpearProjectile.HitTargetCheck = Projectile.HitCheckPierce

function SucubaSpearProjectile:GameInit()
  self:SetScale(120)
end

DefineClass.SA_Spear_Throw = {
  __parents = {
    "SA_Tome_BaseAttack"
  },
  name = T({"Throw"}),
  description = T({"Throws the spear and enemies hit are pushed back."}),
  icon = CurrentModPath.."hud_spear_03.tga",
  cooldown = 5000,
  cooldown_id = "SpearThrow",
  statename = "Spear_Throw",
  adm = 200,
  target_trigger = "PushBack",
  range_attack = true,
  projectile_class = "SpearProjectile",
  fx_action_base = "Hammer_Attack",
  fx_weapon_actor = true,
  attack_weapon = "Hammer",
  weapon = "Hammer",
  weapon_spot = "Weapon1",
  follow_target = true
}
function SA_Spear_Throw:Start(obj)
  local pos = GetMapCursor(obj, self)
  if pos then
    obj:Face(pos)
  end
end
function SA_Spear_Throw:Hit(obj)
  obj:GetActiveWeaponObj():SetOpacity(0)
  local weapon = obj:GetActiveWeapon()
  if weapon and weapon:IsKindOf("Spear_Sucuba") then
  	NetPlaceProjectile("SucubaSpearProjectile", obj)
  else
  	NetPlaceProjectile(self.projectile_class, obj)
  end
end
function SA_Spear_Throw:End(obj)
  obj:GetActiveWeaponObj():SetOpacity(100)  
end
