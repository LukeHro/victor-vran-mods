function BaseInventory:DestroyItem(item, listtype, idx)
  item = item or self.dragging
  local ss = GetLocalWorld(self.dataset.loc_player)
  if not item.can_destroy or not ss.hub then
    self:OnCancelDrag()
    self:EndDrag()
    PlayFX("ItemDenial", "")
    return
  end
  PlayFX("ItemRemove", "")
  self:CreateThread("destroy item", function()
    self:Close()
    local obj = RenameWeaponDlg:new()
    obj.item = item
    obj.player = self.dataset.loc_player
    self.idInventoryList:SetRollover(idx or 1)
    XInputEvent = false
    self:EndDrag()
  end)
end

DefineClass.RenameWeaponDlg = {
  __parents = {
    "LoginCommonDlg"
  },
  new_name = ""
}
function RenameWeaponDlg:Init()
  DataInstances.UIDesignerData.MenuOnline:InitDialogFromView(self, "InGameLogIn")
  self.idEdit:SetZOrder(100)
  self:InitControls()
  self.dataset.main_menu_list_mode = "options"
end
function RenameWeaponDlg:InitControls()
  if Platform.console then
    SetSubviewVisible(self, "in_game", false, "force")
    self.idList:SetVisible(false, "force")
  else
    function self.idList.list.OnContentUpdated(this)
      this:SetSelection(1, true, true, true)
      self.idList:ShowEditCtrl()
    end
  end
  self.idTitle:SetText(T({"Rename weapon"}))
  self.idList:SetFocus()
  self.idList.list:SetSpacing(point(0, 10))
  self.idList:InitControlsFromDataset(RenameWeaponActions)
  self.idList:InitEditCtrl(self.idEdit)
  if GetUIStyleGamepad() and (Platform.steam or Platform.ps4 or Platform.durango or Platform.switch) then
    self:CreateThread(function()
      local result, cancelled, shown = WaitControllerTextInput("", T({"Rename weapon"}), T({"Rename weapon"}), 50)
      if not shown then
        return
      end
      if cancelled or not result or string.len(result) == 0 then
        self:GoBack()
        return
      end
      self.new_name = result
      local ctrls = RenameWeaponActions.ctrls
      for i = 1, #ctrls do
        local ctrl = ctrls[i]
        if ctrl.id == "idRename" then
          ctrl.press_ctrl(self)
        end
      end
    end)
  end
end
function RenameWeaponDlg:GoBack()
  if self.dataset.online_redeem_key then
    self:delete("MainMenu")
  else
    self:delete("idBack")
  end
end
function RenameWeaponDlg:OnKbdKeyDown(char, virtual_key)
  if virtual_key == const.vkEnter then
    self.idList:CallItemFunc("OnLButtonDown", "idRename", "text")
    return "break"
  end
  if virtual_key == const.vkEsc then
    local item_idx = table.find(self.idList.list.items, "id", "idClose")
    if item_idx then
      self.idList.list.item_windows[item_idx].text:OnLButtonDown()
      return "break"
    end
  end
  return LoginCommonDlg.OnKbdKeyDown(self, char, virtual_key)
end
function RenameWeaponDlg:OnXButtonDown(button, controller_id)
  if button == "B" then
    local item_idx = table.find(self.idList.list.items, "id", "idClose")
    if item_idx then
      self.idList.list.item_windows[item_idx].text:OnLButtonDown()
      return "break"
    end
  end
  return AnimTextCtrlsList.OnXButtonDown(self.idList, button, controller_id)
end
RenameWeaponActions = {
  ctrls = {
    {
      id = "idNewName",
      keep_text_in_loc_kit = T({"New Name"}),
      left_text = "",
      right_text = function(menu)
        return Untranslated(menu.new_name:trim_spaces())
      end,
      right_text_font = "OnlineListItemInput",
      member = "new_name",
      press_ctrl = function(menu)
      end,
      bKeepEditMouseCtrl = true,
      edit = true,
      edit_max_chars_size = 50,
      edit_fullsize = true
    },
    {
      text = "",
      bUnselectable = true,
      press_ctrl = function()
      end
    },
    {
      id = "idRename",
      text = T({"Rename"}),
      press_ctrl = function(menu, list, edit_value)
        RemoveItemFromInventory(menu.player, menu.item)
        local ret = menu.item:Clone()
        ret.name = Untranslated(menu.new_name:trim_spaces())
        if ret.mod3 and ret.mod3~="none" and ret:GetRarity() < 4 then
          ret.rarity = 4
        end
        LootItem(menu.player, ret, false)
        menu.item:OnDestroy()
        SaveLocalPlayer(menu.player, "inventory")
        menu:GoBack()
      end
    },
    {
      id = "idClose",
      text = T({3641, "Cancel"}),
      press_ctrl = function(menu, list, edit_value)
        menu:GoBack()
      end
    }
  }
}

function Inventory:RenameWeapon(item)
  self:CreateThread("rename item", function()
    self:Close()
    local obj = RenameWeaponDlg:new()
    obj.item = item
    obj.player = self.dataset.loc_player
    self.idInventoryList:SetRollover(idx or 1)
    XInputEvent = false
    self:EndDrag()
  end)
end


local AddSecondRowHints_Inventory = Inventory.AddSecondRowHints
function Inventory:AddSecondRowHints(item, hint_table, ...)
  local hints = AddSecondRowHints_Inventory(self, item, hint_table, ...)
  local idx = table.find(hint_table, "HintEquip")
  if idx then
    local list = self.idInventoryList
    local sel = list.rollover
    if sel and list.items[sel] and list.items[sel].item and list.items[sel].item:IsKindOf("Weapon") then
      table.insert(hints, "HintRename")
    end
  end
  return hints
end

local OnKbdKeyDown_Inventory = Inventory.OnKbdKeyDown
function Inventory:OnKbdKeyDown(char, virtual_key, repeated)
  if virtual_key == const.vkR then
    local list = self.idInventoryList
    local sel = list.rollover
    if sel and list.items[sel] and list.items[sel].item and list.items[sel].item:IsKindOf("Weapon") then
      self:RenameWeapon(list.items[sel].item)
    end
    return "break"
  else
    return OnKbdKeyDown_Inventory(self, char, virtual_key, repeated)
  end
end


function OnMsg.ClassesBuilt()
  InventoryHintTable.HintRename = {
    text = T({"Rename"}),
    key = const.vkR,
    xbox = "RightThumbClick"
  }
end
