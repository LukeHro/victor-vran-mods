function Outfit_Gentleman:TickOverdrive(obj, state, tick)
  if not state.decay_start_value or not state.decay_start_time then
    table.clear(state)
  end
  local decay = self.overdrive_decay_rate
  local delay = self.overdrive_decay_start/5
  if obj.overdrive_time_changed and GameTime() > obj.overdrive_time_changed + delay then
    if not state.decay_start_time then
      state.decay_start_value = obj.overdrive
      state.decay_start_time = now()
      state.last_overdrive = false
    elseif obj.overdrive <= 0 then
      table.clear(state)
    else
      local t = now() - state.decay_start_time
      local delta = (state.last_overdrive or obj.overdrive) - (state.decay_start_value - decay * t / 1000)
      if state.last_overdrive ~= obj.overdrive then
        state.decay_start_value = obj.overdrive
        state.decay_start_time = now()
        state.last_overdrive = false
      end
      local overdrive = obj.overdrive - delta
      obj:SetOverdrive(overdrive)
      local health = Clamp(obj.health + MulDivRound(delta, self:GetModifierValue("heal_rate"), 100), 0, obj.MaxHealth)
      obj:SetHealth(health)
      state.last_overdrive = overdrive
    end
  else
    table.clear(state)
  end
end

function Outfit_Gentleman:GetDescription(obj)  
  return T({
    self.description,
    value = self:GetModifierValue("heal_rate")
  })
end

---------------------------------------

function Outfit_Samurai:TickOverdrive(obj, state, tick, ...)
  Outfit.TickOverdrive(self, obj, state, tick, ...)
end

function Hero:Damage(health_change, obj, ...)
  local prev_health = self.health
  local new_health = Unit.Damage(self, health_change, obj, ...)
  local outfit = self:GetOutfitItem()
  if health_change < 0 and outfit and outfit:IsKindOf("Outfit_Samurai") then
    local delta = prev_health - new_health - health_change
    self:GainOverdrive(delta, outfit:GetModifierValue("heal_rate"))
  end
  return new_health
end

local function SamuraiCanUse(self, vanilla_func, obj)
  if obj.items and IsKindOf(obj.items[OutfitIdx], "Outfit_Samurai") then
    return vanilla_func(self, obj) or (obj.health <= obj.MaxHealth)
  else
    return vanilla_func(self, obj)
  end
end

local CanUse_SA_Heal = SA_Heal.CanUse
function SA_Heal:CanUse(obj) 
  return SamuraiCanUse(self, CanUse_SA_Heal, obj)
end

local CanUse_SA_Potion_MedKit = SA_Potion_MedKit.CanUse
function SA_Potion_MedKit:CanUse(obj)
  return SamuraiCanUse(self, CanUse_SA_Potion_MedKit, obj)
end

local CanUse_SA_Potion_Ambrosia = SA_Potion_Ambrosia.CanUse
function SA_Potion_Ambrosia:CanUse(obj)
  return SamuraiCanUse(self, CanUse_SA_Potion_Ambrosia, obj)
end

local Tick_Buff_Regeneration = Buff_Regeneration.Tick
function Buff_Regeneration:Tick(unit)
  if unit.items and IsKindOf(unit.items[OutfitIdx], "Outfit_Samurai") then
    if unit.health > 0 then
      local outfit = unit:GetOutfitItem()
      local hp = MulDivRound(self.hps, self.tick_time, 1000) + MulDivRound(unit.MaxHealth, self.health_prom, 1000)
      local regen = Min(unit.MaxHealth - unit.health, hp)
      if regen > 0 then
        unit:SetHealth(unit.health + regen)
      end
      unit:GainOverdrive(hp - regen, outfit:GetModifierValue("heal_rate"))
    end
    return self.tick_time
  else
    return Tick_Buff_Regeneration(self, unit)
  end
end

function Outfit_Samurai:GetDescription(obj)  
  return T({
    self.description,
    value = self:GetModifierValue("heal_rate")
  })
end

---------------------------------------

function Outfit_Trooper:TickOverdrive(obj, state, tick, ...)
  Outfit.TickOverdrive(self, obj, state, tick, ...)
end

local GainOverdrive_Hero = Hero.GainOverdrive
function Hero:GainOverdrive(amount, modifier)
  local initial_overdrive = self.overdrive
  GainOverdrive_Hero(self, amount, modifier)
  local end_overdrive = self.overdrive
  if amount > 0 and end_overdrive == self.MaxOverdrive then
    local outfit = self:GetOutfitItem()
    if outfit:IsKindOf("Outfit_Trooper") then
      local delta = MulDivRound(end_overdrive - initial_overdrive - amount, outfit:GetModifierValue("heal_rate"), 100)
      self:Damage(delta, outfit)
    end
  end
end

function Outfit_Trooper:GetDescription(obj)  
  return T({
    self.description,
    value = self:GetModifierValue("heal_rate")
  })
end

---------------------------------------

function Outfit_Wanderer:OnDamageDone(obj, damage, target, action, unmodified_damage, critical_hit, ...)
  if self:IsKindOf("Outfit_Gentleman") or self:IsKindOf("Outfit_Samurai") or self:IsKindOf("Outfit_Trooper") then
    Outfit.OnDamageDone(self, obj, damage, target, action, unmodified_damage, critical_hit, ...)
  end
end

function OnMsg.ClassesBuilt()
  Outfit_Gentleman.mod2 = "heal_rate"
  Outfit_Gentleman.modifiers_0 = {armor = 10, heal_rate = 50}
  Outfit_Gentleman.modifiers_100 = {armor = 50, heal_rate = 150}
  Outfit_Gentleman.description = T({
    11070101,
    "You gain Health as Overdrive decays at <lightgreen><value>%</lightgreen> rate, Overdrive decay starts faster when out of combat."
  })
  Outfit_Gentleman.codex_description = T({
    11070102,
    "The Cavalier's Outfit grants Health as Overdrive decays, Overdrive decay starts sooner when out of combat."
  })
  Outfit_Samurai.mod2 = "heal_rate"
  Outfit_Samurai.modifiers_0 = {armor = 10, heal_rate = 50}
  Outfit_Samurai.modifiers_100 = {armor = 50, heal_rate = 150}
  Outfit_Samurai.description = T({
    11070103,
    "You gain Overdrive when healing if already at full health, at <lightgreen><value>%</lightgreen> rate."
  })
  Outfit_Samurai.codex_description = T({
    11070104,
    "The Samurai's Outfit grants Overdrive when healing if already at full health."
  })
  Outfit_Trooper.mod2 = "heal_rate"
  Outfit_Trooper.modifiers_0 = {armor = 10, heal_rate = 50}
  Outfit_Trooper.modifiers_100 = {armor = 50, heal_rate = 150}
  Outfit_Trooper.description = T({
    11070105,
    "You gain Health when Overdrive would increase but is full, at <lightgreen><value>%</lightgreen> rate."
  })
  Outfit_Trooper.codex_description = T({
    11070106,
    "The Trooper's Outfit grants Health when Overdrive would increase but is full."
  })
end
