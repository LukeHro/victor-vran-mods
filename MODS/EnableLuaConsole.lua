Platform.developer = true
dofile("CommonLua/console.lua")
Platform.developer = false

UserActions.AddActions({LH_Console={
	key = "~",
	description = "Shows console",
	action = function()
		ShowConsole(true)
	end
	}
})

local my_rules = {
	{"^~(.*)", "OpenExamine((%s))"},
	{"^`(.*)", "dlgConsoleLog:AddLogText('', true) dlgConsoleLog:AddLogText(print_format(%s), true)"},
	{"^-(.*)", "dlgConsoleLog:ClearText()"}
}

ConsoleExecVanilla = ConsoleExec
ConsoleExec = function (text, rules,...)
	return ConsoleExecVanilla(text, my_rules,...)
end

function OnMsg.ClassesBuilt()
	ShowConsoleLog(true) 
end