# VictorVranMods

Project contains a light mod manager for Victor Vran capable to load lua files placed in folder MOD located in the game folder ([Steam Library]\steamapps\common\Victor Vran)

The mod manager loads lua files found inside the MOD folder (should be created in the game folder). Lua files can have any name and can be grouped in folders, I recommend one base folder per mod.



Contents:

lh_modmanager_SOURCES - the source files of the mod manager, not needed for the manager and the mods to work

DLC - contains the mod manager packed as hpk file, the lh_modmanager.hpk should be placed in DLC folder in the game's folder

MODS - contains my mods, the whole MODS folder should be copied in the game folder. All mods that can use my loader, by any author, should be placed here.



Mods:

EnableLuaConsole.lua - Allows opening the Lua console with the ` key. Special shortcuts in console: ~ opens examine window for the result of the function/table that follows, ` logs in the game window the result of the function/table, - clears the in game log

NewOutfitEffects.lua - The Cavalier's Outfit grants Health as Overdrive decays, Overdrive decay starts sooner when out of combat. (Wanderer, Samurai, Trooper outfits have the vanilla effect and can be aquired using "Gun's SWaT Mod"...but I may create other unique effects for two of them too)

