
local function DofileIntoEnv(filename, env, path)
    setmetatable ( env, { __index = _G } )
    rawset(env, "CurrentModPath", path)
    dofile(filename, env)
    --setmetatable(env, nil)
    return env
end

local function LH_LoadMods()
  if not io.exists("MODS") then
    DebugPrint("No MODS folder found")
    return
  end
  local files = io.listfiles("MODS/", "*.hpk", "recursive") or {}
  for i = 1, #files do
    DebugPrint(string.format("Loading mod pack...%s\n",files[i]))
    local pack = string.gsub(files[i], ".hpk", "")
    local err = MountPack(pack, files[i])
    if err then
      DebugPrint("Error "..err)
    else
      local luas = io.listfiles(pack.."/", "*.lua", "recursive") or {}
      local env = {}
      for j = 1, #luas do
        DebugPrint(string.format("Loading packed lua...%s\n",luas[j]))
        DofileIntoEnv(luas[j], env, pack.."/")
        --dofile(luas[i])
      end
    end
  end
  local files = io.listfiles("MODS/", "*.lua", "recursive") or {}
  for i = 1, #files do
    DebugPrint(string.format("Loading lua code...%s\n",files[i]))
    dofile(files[i])
  end
  FlushLogFile()
end

local LH_ReloadLua = function(dlc)
  PauseInfiniteLoopDetection("ReloadLua")
  local ct = CurrentThread()
  ReloadForDlc = dlc or false
  print("Reloading lua files")
  Msg("ReloadLua")
  collectgarbage("collect")
  dofile("CommonLua/Core/autorun.lua")
  LH_LoadMods()
  Msg("Autorun")
  print("Reloading done")
  ReloadForDlc = false
  if ct then
    InterruptAdvance()
  end
  ResumeInfiniteLoopDetection("ReloadLua")
end

ReloadLua = function(dlc)
  if dlc then 
    DebugPrint("Skiping default dlc reloading")
    return 
  end
  LH_ReloadLua()
end

local loaded_dlcs
local DlcAddMusic_vanilla = DlcAddMusic
function DlcAddMusic(dlcs)
  loaded_dlcs = dlcs
end

return {
  name = "lh_modmanager",
  pre_load = function()
  end,
  post_load = function()
    g_AvailableDlc.lh_modmanager = true    
    DebugPrint("Loading dlc and mod lua...")
    LH_ReloadLua(true)
    DebugPrint("Loaded dlc and mod lua...")
    DlcAddMusic_vanilla(loaded_dlcs)
  end
}